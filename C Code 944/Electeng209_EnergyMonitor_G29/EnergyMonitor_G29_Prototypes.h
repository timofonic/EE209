/********************************************
 *************** Prototypes29 ***************
 *------------------------------------------*
 *************** Electeng 209 ***************
 **************** Group: 29 ****************/ 


#ifndef EnergyMonitor_G29_Prototypes
#define EnergyMonitor_G29_Prototypes

// ************************* UART & TRANSMISSION ***************************
// *************************************************************************

// UART Initialisation Function Declaration
void UART_Init(unsigned int ubrr_val);

// UART Transmission Function Declaration
void UART_Transmit(uint8_t data);

// Password Frame Function Declaration
void PasswordTrans(void);

// Make Array and Transmit Function Declaration
void MakeArray_Trans(float Num);

// Frame format Function Declaration
uint8_t Frame_format(uint8_t data, uint8_t decimal, uint8_t display);

// Encryption Function Declaration
uint8_t Encrypt_Data(uint8_t data);


// ***************************** ADC & OTHER *******************************
// *************************************************************************

// ADC Initialisation Function Declaration
void ADC_Init(void);

// ADC Function Declaration
uint16_t get_ADCVal(uint8_t ADC_n);

// Calculate ADC Reading Function Declaration
float Calc_ADCVin(uint16_t number);

// Decimal point Detect Function Declaration
uint8_t decimalPt_detect(float ADC_value);

// LED-B5 Blink Function Declaration
void blink_LED(void);


// ************************* CONTROL FUNCTIONS *****************************
// *************************************************************************

// Power, Voltage, Current Control Function Declaration
void PwrVltCrnt_control(uint8_t ctrl_operator);


// ******************************** CURRENT ********************************
// *************************************************************************

// Overall Current Function Declaration
void Currentfunct(void);

// Current Pin Selection Function Declaration
uint8_t CurrentPinSelect(uint8_t pin);

// Current Calculation Function Declaration
float Current_calc(float ADC_reading, uint8_t pin);

//Peak Current Calculation Declaration
float CurrentMax(uint8_t pin);


// ****************************** VOLTAGE **********************************
// *************************************************************************

// Voltage Calculation Function Declaration
float Voltage_calc(float ADC_reading);

// Overall Voltage Function Declaration
void Voltagefunct(void);


// ********************************* POWER *********************************
// *************************************************************************

// Overall Power Function Declaration
void Powerfunct(void);


// ********************************* TIMERS ********************************
// *************************************************************************

//Select a certain time delay for Timer1
void setTimerDelay(uint8_t count);

//Initialise Timer1
void Init_Timer1(void);

//
void setCycleTimer(uint8_t cycles);


// *************************** FREQUENCY & PHASE ***************************
// *************************************************************************

//Global variable for frequency of signal
volatile float freq;

//Calculate frequency of signal
float CalculateFrequency(uint8_t channel);

//
float CalculatePhaseShift(float freq);

float SamplePhaseShift(void);


// ********************************* OTHER *********************************
// *************************************************************************

uint8_t *address;
uint16_t eepVal;
uint16_t energy;
uint16_t max_address;

//Initialise Analog Comparator
void Init_Analog_Comp(uint8_t channel);

// RMS Value calculation Function Declaration
float RMSval(uint8_t pin, uint8_t identify);

// EEPROM reset Function Declaration
void eeprom_reset(void);

// EEPROM energy log Function Declaration
void eeprom_add(uint16_t energy);

// Energy Calculation Function Declaration
uint16_t EnergyCalc(float powerVal);

// Energy Calculation, Transmission and Storage function
void Energyfunct(void);


#endif