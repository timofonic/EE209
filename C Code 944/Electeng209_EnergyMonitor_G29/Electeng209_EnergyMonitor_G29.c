/********************************************
 ******** Electeng209_MicroCode_G29 *********
 *------------------------------------------*
 *************** Electeng 209 ***************
 **************** Group: 29 ****************/


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/eeprom.h>
#include "EnergyMonitor_G29_Prototypes.h"    // Including the header file of functions
#define F_CPU 16000000UL                     // Defining the oscillator frequency- 16 MHz
#define BAUD 9600UL                          // Defining the baud rate	
#define PC1_Vsen 1
#define PC3_IsenHG 3
#define PC5_IsenLG 5				 
#define UBRR_VAL ((F_CPU/(BAUD*16))-1)       // Defining the UBRR0's value


// EEPROM reset ISR, triggered whenever the button is pressed
ISR(PCINT0_vect)
{
	// Disable all interrupts
	cli();
	
	// While the button hasn't been pressed, keep looping.
	// If the button is pressed, then break out of this
	// loop.
	while (1)
	{
		if (PINB & (1<<PINB7))
		{
			break;
		}
	}

	// Enable global interrupts and then reset the EEPROM
	// using the written function.
	sei();
	eeprom_reset();
}

int main(void)
{
	//Disable global interrupts
	cli();
	// Initialise the UART
	UART_Init(UBRR_VAL);
	
	// Initialise the ADC
	ADC_Init();
	
	// Telling the Micro that LED-B5 is an output pin
	DDRB |= (1<<5);
	DDRB &= ~(1<<DDB7);
	
	// Initialising the values for the EEPROM global variables
	address = 0;
	eepVal = 0;
	energy = 0;
	max_address = 1024;
	
	// While the button hasn't been pressed
	while(PINB & (1<<PINB7));
	
	// Sending the password frame
	PasswordTrans();
	
	
	// Calculating frequency of input signal
	freq = CalculateFrequency(PC1_Vsen);
	
	// If frequency gives an outlier - set it to 600Hz default
 	if(freq > 660 || freq < 540)
  	{
  		freq = 600;
  	}
	
	// Enable global interrupts
	sei();
	
	// Enable interrupts on pin change
	PCICR |= (1<<PCIE0);
	
	// Set up the change on PB7, to trigger an interrupt
	PCMSK0 |= (1<<PCINT7);
	
	while(1)
	{		
		// Control loop for cycling through and calculating
		// power, voltage and current, and then transmitting
		// them with their proper frame identifiers
  		uint8_t oppI;
  		
   		for (oppI = 0; oppI < 4; oppI++)
   		{
    			PwrVltCrnt_control((oppI + 1));
    	}
	}
	
	return 0;
}