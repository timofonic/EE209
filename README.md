# EE209
Source code (VHDL and Embedded C) for ELECTENG 209 Design Project - Wireless Energy Meter

As part of my ELECTENG 209 Course, we are reuired to design and implement a Wireless Energy meter that combines analogue circuitry, an Atmel AVR microcontroller and a MAX7000E CPLD.
This repository contains the Embedded C code and VHDL that will be used.

Due date: 21st October
