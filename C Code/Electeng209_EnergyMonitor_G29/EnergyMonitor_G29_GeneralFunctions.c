/********************************************
 ************** Functions_G29 ***************
 *------------------------------------------*
 *************** Electeng 209 ***************
 **************** Group: 29 ****************/

#define F_CPU 16000000UL  
#include <avr/io.h>
#define ADC_RES 1024U
#define AREF 5
#define VREF 1.65F
#include <util/delay.h>
#include "EnergyMonitor_G29_Prototypes.h"    // Including the header file of functions
#include <avr/interrupt.h>
#include <stdbool.h>
#include <math.h>


// ************************* UART & TRANSMISSION ***************************
// *************************************************************************

// UART Initialisation Function Definition
void UART_Init(unsigned int ubrr_val)
{
	// Setting the UBRR0 value using its High and Low registers
	UBRR0H = (ubrr_val>>8);
	UBRR0L = ubrr_val;
	
	// Enabling the USART receiver and transmitter
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0);
	
	// Setting the frame format to 9 bit data,
    // with the default 1 stop bit
	UCSR0C |= (1<<UCSZ00) | (1<<UCSZ01);
    //UCSR0B |= (1<<UCSZ02);
}

// UART Transmission Function Definition
void UART_Transmit(uint8_t data)
{
	// Check that the USART Data Register is empty, AND if UCSR0A
	// is all 0s
	while(!(UCSR0A & (1<<UDRE0)));
	
	// Encrypt the data first, then send it
	uint8_t TX_data = Encrypt_Data(data);
	
	// Since UDR is empty put the data we want to send into it,
	// then wait for a second and send the following data
	UDR0 = TX_data;
	
}

// Password Frame Function Definition
void PasswordTrans(void)
{
	// Setting the password.
	uint8_t PassWd = 0b00011101;
	
	// Setting the Timer to loop for 1 second
	setTimerDelay(1);
	
	while(1)
	{
		// If the overflow flag has been raised, then the 1 second
		// must be over, so break out of this statement
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		
		// While it is still under 1 second, transmit the Password
		UART_Transmit(PassWd);
	}
}

// Make Array and Transmit Function Definition
void MakeArray_Trans(float Num)
{
	// Initialising Variables
	uint8_t NumArray[] = {0,0,0,0};
	uint8_t TX_data;
	uint8_t i, j;
	uint8_t DispSel = 4;
	uint32_t a;
	uint32_t b = 0;
	
	// First detecting where the decimal point on this number is
	uint8_t DecimPtPlace = decimalPt_detect(Num);
	
	// Multiplying the value by 1000 to prepare it to be stored into the
	// array, and also to select the appropriate b value
	uint32_t Val = (uint32_t)(Num * 1000);
	
	// These conditionals start off with a different initialisation 
	// for b depending on the magnitude of the number being sent.
	// This first loop is for numbers less than 10,000, i.e values less
	// than 10 (all single digit values).
	if (Val < 10000)
	{
		b = 1000;
	}
	// This second loop is for numbers less than 100,000, but greater than
	// or equal to 10,000, i.e values less than 100 but 10 or more (all
	// double digit values).
	else if ((Val >= 10000) && (Val < 100000))
	{
		b = 10000;
	}
	// This last loop does the same but for values less than 1,000,000, but 
	// greater than 100,000, i.e values less than 1000, but 100 or more (all
	// triple digit values).
	else if ((Val >= 100000) && (Val < 1000000))
	{
		b = 100000;
	}	
	
	// This loop will go through, and break up the number of Val into its
	// digits, and then store them into an array. This array will then be
	// read and each element will be sent out.
	for (i = 0; i < 4; i++)
	{
		a = Val % b;
		NumArray[i] = (Val - a) / b;
		b = b / 10;
		Val = a;
	}
		
	// This loop will transmit each element in the array of values we
	// made which are the digits of the number being sent.
	for (j = 0; j < 4; j++)
	{
		// This loop will check if the current display needs to have its
		// decimal point activated, but seeing if the calculated decimal
		// point place is the same as the current display's. If so, then
		// it simply tels frame format that this frame needs its decimal
		// activated.
		if (DecimPtPlace == DispSel)
		{
			TX_data = Frame_format(NumArray[j], 1, DispSel);
		}
		else
		{
			TX_data = Frame_format(NumArray[j], 0, DispSel);
		}
			
		// Transmit the created frame via the initialised UART, then
		// decrement the display value to the next one.
		UART_Transmit(TX_data);
		DispSel--;
	}
}

// Frame Format Function Definition
uint8_t Frame_format(uint8_t data, uint8_t decimal, uint8_t display)
{
    //Initalising an empty frame.
    uint8_t Frame_data = 0;
	
	// Add the data to be displayed into the empty frame
	// depending on its value, according to the new frame format
	if ((data < 4) && (data >= 0))
	{
		Frame_data |= data;
	}
	else
	{
		switch (data)
		{
			case 4:
				Frame_data |= 32;
				break;
			case 5:
				Frame_data |= 33;
				break;
			case 6:
				Frame_data |= 34;
				break;
			case 7:
				Frame_data |= 35;
				break;
			case 8:
				Frame_data |= 64;
				break;
			case 9:
				Frame_data |= 65;
				break;
		}
	}
    
    // Check if there is a decimal, and then add 16 to turn
    // decimal on
    switch (decimal)
    {
        case 1:
            Frame_data |= (Frame_data | 4);
            break;
        default:
            break;
    }
    
    // Select the relevant display for the number, if the
    // display is Disp1, then do nothing
    switch (display) {
        // Display 4 activate
        case 4:
            Frame_data |= (Frame_data | 136);
            break;
        // Display 3 activate
        case 3:
            Frame_data |= (Frame_data | 128);
            break;
        // Display 2 activate
        case 2:
            Frame_data |= (Frame_data | 8);
            break;
        // Display 1 activate, as a default for single digits
        default:
            break;
    }
	
	return Frame_data;
}

// Encryption Function Definition
uint8_t Encrypt_Data(uint8_t data)
{
	// Declaring a key for the XOR cipher. Then
	// XORing the data with the key, then returning
	// it.
	uint8_t key = 0b00101001;
	uint8_t encrypted_data = data ^ key;
	
	return encrypted_data;
}


// ***************************** ADC & OTHER *******************************
// *************************************************************************

// ADC Initialisation Function Definition
void ADC_Init(void)
{
    // Setting the Vcc as the reference voltage.
    ADMUX |= (1<<REFS0);
    
    // Enabling the ADC and the ADC automatic trigger. We also are
	// setting the prescaler to 64 for a frequency of 250k Hz.
    ADCSRA |= (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1);
}

// ADC Function Definition
uint16_t get_ADCVal(uint8_t ADC_n)
{
	// Resetting the entire MUX so that we get the right channel.
    ADMUX &= ~(1<< MUX0) & ~(1<<MUX1) & ~(1<<MUX2) & ~(1<<MUX3);
	
	// Telling the MUX which channel we are wanting to
	// read from.
    ADMUX |= ADC_n;
    
	// Turning on the Start Conversion Control bit
    ADCSRA |= (1<<ADSC);
    
	// While the ADC conversion complete interrupt hasn't
	// been activated yet, this loop holds up the program. 
	// As soon as the convesrion is complete, the flag will
	// be set to 1, and the while will be broken out of, so
	// returning the ADC converted value.
    while(!(ADCSRA & (1<<ADIF)));
	
	// Clearing the ADIF
	ADCSRA |= (1<<ADIF);
	
	// Return the converted value
	return ADC;
	
}

// Calculate ADC Reading Function Definition
float Calc_ADCVin(uint16_t number)
{
	// First calculating the voltage that corresponds to the
	// ADC value, then multiplying it by 1000 to allow it to
	// be stored into an array for transmission
	float ConVal = ((float)number/ (float)ADC_RES) * ((float)AREF);
	return ConVal;
}

// Decimal point Detect Function Definition
uint8_t decimalPt_detect(float ADC_reading)
{
	// Get the value from the calculate functions that's been
	// multiplied by 1000, and convert it back to the float it started
	// as. Then cast it to an int to only get the numbers before the
	// decimal point.
	int num = (int)(ADC_reading);
	
	// Initialising variables
	int numdigits = 0;
	int decimalptplace = 0;
	
	// If the casted number is below 10, it can only have had 1 digit.
	if (num < 10)
	{
		numdigits = 1;
	}
	// If the casted number is below 100 and 10 or higher, then it can
	// only have 2 digits.
	else if ((num < 100) && (num >= 10))
	{
		numdigits = 2;
	}
	// If the casted number is below 1000 and 100 or higher, then it can
	// only have 3 digits.
	else if ((num < 1000) && (num >= 100))
	{
		numdigits = 3;
	}
	
	// Calculating which display this decimal point will be on
	// either Display 4, 3 or 2. We wont be working with values greater
	// than these, so display 1's decimal point will never be turned on.
	decimalptplace = 5 - numdigits;
	
	// Return the display number where the decimal point will be enabled on.
	return decimalptplace;
}

// LED-B5 Blink Function Definition
void blink_LED(void)
{
	// Write a high to port B5, then wait for half a second
	PORTB |= (1<<5);
	while(1){
		if(TIFR1 & (1<<TOV1)){
			TIFR1 |= (1<<TOV1);
			break;
		}
	}
	// Write a low to port B5, then wait half a second
	PORTB &= ~(1<<5);
	while(1){
		if(TIFR1 & (1<<TOV1)){
			TIFR1 |= (1<<TOV1);
			break;
		}
	}
}	

