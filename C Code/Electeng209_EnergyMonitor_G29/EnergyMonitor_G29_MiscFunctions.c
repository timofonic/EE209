/********************************************
 ************ MiscFunctions_G29 *************
 *------------------------------------------*
 *************** Electeng 209 ***************
 **************** Group: 29 ****************/


#define F_CPU 16000000UL  
#include <avr/io.h>
#define ADC_RES 1024U
#define AREF 5
#define VREF 1.65F
#include <util/delay.h>
#include "EnergyMonitor_G29_Prototypes.h"    // Including the header file of functions
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdbool.h>
#include <math.h>

// ********************************* TIMERS ********************************
// *************************************************************************

// Timer Delay Function Definition
void setTimerDelay(uint8_t count)
{
	// Sets up timer to have a delay of count seconds
	
	//Disable all Timer1 Interrupts
	TIMSK1 &= ~(1<<TOIE1);
	
	//Clear overflow flag
	TIFR1 |= (1<<TOV1) | (1<<ICF1);
	
	//Ensure counter is stopped
	TCCR1B &= ~(1<<CS12) & ~(1<<CS11) & ~(1<<CS10);  
		
	//Reset count according to time delay requested
	uint16_t reset = 65535 - (count * F_CPU/1024);
	TCNT1 = reset;
	
	//Start count with pre-scaler of 1024
	TCCR1B |= (1<<CS12) | (1<<CS10);
}

// Timer1 Initialisation Function Definition
void Init_Timer1(void){
	
	
	//Disable all Timer1 Interrupt
	TIMSK1 &= ~(1<<ICIE1) & ~(TOIE0);
	
	//Ensure counter is stopped
	TCCR1B &= ~(1<<CS12) & ~(1<<CS11) & ~(1<<CS10);
	
	//Reset count
	TCNT1 = 0x0000;
	
	//Enable input capture noise canceler
	TCCR1B |= (1<<ICNC1);
}

//Set timer delay by "count" number of cycles (maximum delay of 9 cycles)
void setCycleTimer(uint8_t cycles)
{
	
	//Disable all Timer0 Interrupts
	TIMSK0 &= ~(1<<TOIE0);
	
	//Clear overflow flag
	TIFR0 |= (1<<TOV0);
	
	//Ensure counter is stopped
	TCCR0B &= ~(1<<CS02) & ~(1<<CS01) & ~(1<<CS00);
	
	//Reset count according to time delay requested
	uint16_t reset = 255 - ((cycles/freq) * (F_CPU/1024));
	TCNT0 = reset;
	
	//Start count with pre-scaler of 1024
	TCCR0B |= (1<<CS02) | (1<<CS00);
}


// *************************** FREQUENCY & PHASE ***************************
// *************************************************************************

// Frequency Calculate Function Definition
float CalculateFrequency(uint8_t channel)
{
	float freqVal;
	uint16_t count;
	bool wait = true;
	
		
	// Initialize Analog Comparator and Timer1
	Init_Timer1();
	Init_Analog_Comp(channel);
	
	// WAIT until analog comparator detects a zero crossing
	while(wait)
	{
		if(ACSR & (1<<ACI))
		{
			wait = false;
		}
	}
	// Clear Analog Comparator Interrupt flag
	ACSR |= (1<<ACI);
	
	// Start count with no pre-scaler
	TCCR1B |= (1<<CS10);
	
	// Set falling edge as capture trigger
	TCCR1B &= ~(1<<ICES1);
				
	// Enable input capture with Timer1
	ACSR |= (1<<ACIC);
	
	wait = true;
	
	/// WAIT until capture is triggered by the next zero crossing
	while(wait)
	{
		if(TIFR1 & (1<<ICF1))
		{
				wait = false;
		}
	}
	
	//Capture the count value stored in Input Capture Register
	count = ICR1;	
	
	//Clear Timer Input Capture Flag
	TIFR1 |= (1<<ICF1);
		
	// Clear Analog Comparator Interrupt flag
	ACSR |= (1<<ACI);
				
	/*Frequency = (clock frequency / pre-scaler)
					  ------------------------------
								timer count           */	
		
	freqVal = ((float)F_CPU)/((float)count*2.0);
	
	//Disable Analog Comparator
	ACSR &= ~(ACSR);

	//Re-enable ADC so MUX channels can be used
	ADCSRA |= (1<<ADEN);
		
	//Analog comparator multiplexer disable
	ADCSRB &= ~(1<<ACME);
		
	//Reset count
	TCNT1 = 0x0000;
		
	//Clear ADC channels
	ADMUX &= ~(1<<MUX0) & ~(1<<MUX1) & ~(1<<MUX2) & ~(1<<MUX3);

	return freqVal;	
}

//Find the phase shift between voltage and current waveform (assume time shift approx. equals phase shift of fundamental)
float CalculatePhaseShift(float freq){
	float phase = 0;
	float sum = 0;
	uint16_t count;
	uint8_t samples = 10;
	bool wait = true;
	
	//Disable global interrupts
	cli();
	//for(int i = 0; i < samples; i++)
	//{
	
		//Initialize Analog Comparator and Timer1
		Init_Analog_Comp(1);
		Init_Timer1();
	
		//WAIT until analog comparator detects a zero crossing
		while(wait)
		{
			if(ACSR & (1<<ACI))
			{
				wait = false;
			}
		}
	
		//Start count with pre-scaler of 8
		TCCR1B |= (1<<CS01);

		//Switch to channel 5 (Current signal)
		ADMUX |= (1<<MUX2) | (1<<MUX0);
	
		//Set rising edge as capture trigger
		TCCR1B |= (1<<ICES1);
	
		//Clear Analog Comparator Interrupt flag
		ACSR |= (1<<ACI);
	
		//Enable input capture with Timer1
		ACSR |= (1<<ACIC);
	
		///WAIT until capture is triggered by the next zero crossing
		while(wait)
		{
			if(TIFR1 & (1<<ICF1)) 
			{
				wait = false;
			}
		}
	
		//Capture the count value stored in Input Capture Register
		count = ICR1;
	
		//Clear Timer Input Capture Flag
		TIFR1 |= (1<<ICF1);

		//Stop Counter
		TCCR1B &= ~(1<<CS12) & ~(1<<CS11) & ~(1<<CS10);
	
		//Disable Analog Comparator
		ACSR &= ~(ACSR);
	
		/*Phase Shift = 2 * PI * frequency * count * (1 / fclk)*/
		phase = 2 * M_PI * freq * (float)count * (1.0/(F_CPU/8.0));
		//sum += phase;
	//}
	
	//Disable ADC so MUX channels can be used
	ADCSRA |= (1<<ADEN);
	
	//Disbale analog comparator multiplexer
	ADCSRB &= ~(1 << ACME);
	
	//Analog comparator multiplexer enable
	ADCSRB &= ~(1 << ACME);
	
	ADMUX &= ~(1<<MUX0) & ~(1<<MUX1) & ~(1<<MUX2) & ~(1<<MUX3);
	
	return phase;
	//return (sum/(float)samples);
}


// ********************************* OTHER *********************************
// *************************************************************************

// Analogue Comparator Initialisation Function Definition
void Init_Analog_Comp(uint8_t channel)
{
	//Clear analog comparator control and status register to ensure everything is initially disabled
	ACSR &= ~(ACSR);
	
	//Disable ADC so MUX channels can be used
	ADCSRA &= ~(1<<ADEN);
	
	//Analog comparator multiplexer enable
	ADCSRB |= (1 << ACME);
	
	ADMUX &= ~(1<<MUX0) & ~(1<<MUX1) & ~(1<<MUX2) & ~(1<<MUX3);
	
	//Set ADMUX to read given channel
	ADMUX |= (1<<MUX0);
	
	//To trigger the interrupt event on a rising edge
	ACSR |= (1<<ACIS1) | (1<<ACIS0) | (1<<ACO);
}

// EEPROM reset Function Definition
void eeprom_reset(void)
{
	// Initialising the
	uint16_t eepVal_Res = 0;
	uint16_t *address_Res;
	
	for (address_Res = 0; address_Res < max_address; address_Res++)
	{
		// If the value at  the address is the same as the reset value
		// of 0, then don't overwrite it. Only overwrite the value
		// to 0 if the value is different.
		uint16_t readVal = eeprom_read_word(address);
		
		if (readVal == eepVal_Res)
		{
			// Do nothing
		}
		else
		{
			eeprom_update_word(address, eepVal_Res);
		}
	}
}

// EEPROM energy log Function Definition
void eeprom_add(uint16_t energyADD)
{
	// If at the end, then loop back to the beginning.
	if (address == max_address)
	{
		// reset global variable of address to 0;
		address = 0;
	}
	
	// Check the value at the address, then add the new energy
	// to it and then store into the next address space.
	uint16_t a = eeprom_read_word(address);
	eepVal = a + energyADD;
	
	// Store the eepVal into this address
	eeprom_update_word(address, eepVal);
	
	// Increment the address value, for the next time it
	// comes to store energy
	address++;
}

// Energy Calculation Function Definition
uint16_t EnergyCalc(float powerVal)
{
	// Calculating energy using the power,
	// and the frequency and number of cycles
	// that we had calculated power over.
	float time = (1/freq)*9;
	uint16_t energyVal = (uint16_t)(powerVal/time);
	
	return energyVal;
}
