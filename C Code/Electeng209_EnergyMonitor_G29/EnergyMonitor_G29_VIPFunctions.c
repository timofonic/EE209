/********************************************
 ************ VIPFunctions_G29 **************
 *------------------------------------------*
 *************** Electeng 209 ***************
 **************** Group: 29 ****************/

#define F_CPU 16000000UL  
#include <avr/io.h>
#define ADC_RES 1024U
#define AREF 5
#define VREF 1.65F
#define G1 1.28598F		// above 1.95 pin3
#define G2 7.51664F		// below 1.95 pin5
#define PC1_Vsen 1
#define PC5_IsenHG 5
#define PC3_IsenLG 3
#include <util/delay.h>
#include "EnergyMonitor_G29_Prototypes.h"    // Including the header file of functions
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdbool.h>
#include <math.h>

// ************************* CONTROL FUNCTIONS *****************************
// *************************************************************************

// Power, Voltage, Current Control Function Definition
void PwrVltCrnt_control(uint8_t ctrl_operator)
{
	// A series of switch/cases to determine weather
	// we are to do a voltage, current, power
	// or energy calculation and transmission

	switch (ctrl_operator)
	{
		case 4:
			// For Energy calculation, transmission
			// and then store to EEPROM
			Energyfunct();
			break;	
		
		case 3:
			// For Power calculation and transmission
			Powerfunct();
			break;
		
		case 2:
			// For Current calculation and transmission
			Currentfunct();
			break;
		
		case 1:
			// For Voltage calculation and transmission
			Voltagefunct();
			break;
		
		default:
			break;
	}
}


// ******************************** CURRENT ********************************
// *************************************************************************

// Current Pin Selection Function Definition
uint8_t CurrentPinSelect(uint8_t pin)
{
	// Initialising variables
	float CrntREF = 1.9;
	uint8_t cycles = 5;
	float max_val = 0;
	
	setCycleTimer(cycles);
	while(1)
	{
		// If the overflow is raised, then the 5 cycles
		// must be finished
		if(TIFR0 & (1<<TOV0))
		{
			TIFR0 |= (1<<TOV0);
			break;
		}	
	
		// Reading from ADC is assigned to a variable
		uint16_t adcVal = get_ADCVal(pin);
		
		// ADC reading is then given to the calculate
		// function which will calculate the original
		// Vin that the ADC received.
		float adcVin = Calc_ADCVin(adcVal);
	
		if (adcVin > max_val)
		{
			// If the Vin of the ADC is larger then the
			// current max val, then replace it with that
			// value.
			max_val = adcVin;
		}
	}
	
	// If the maximum value is less than 1.9, then switch
	// to the higher gain circuit.
	if (max_val < CrntREF)
	{
		// New pin selection due to max value in the sampled
		// waveform being lower than the reference.
		pin = PC5_IsenHG;
	}
	else
	{
		//Use the lower gain circuit as the max value in the
		//sampled waveform is larger than the reference.
		pin = PC3_IsenLG;
	}
	
	return pin;
}

// Overall Current Function Definition
void Currentfunct(void)
{
	// The binary representation of the frame for
	// displaying a "A" on the CPLD.
	uint8_t CurrentFrame = 0b11001011;
	
	// Send the "A" frame for 1 second.
	setTimerDelay(1);
	PORTB |= (1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 1 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		UART_Transmit(CurrentFrame);
	}
	
	
	// Checking if the signal on pin 3 is above a certain
	// value. If it is below that value, then we will change
	// to reading from the higher gain circuit.
	uint8_t adcPin = CurrentPinSelect(PC3_IsenLG);
	float currentPk = CurrentMax(adcPin);
	//float currentRMS = RMSval(adcPin,2);
	
	// Current in mA
	currentPk = currentPk * 1000;
	
	//
	setTimerDelay(2);
	PORTB &= ~(1<<5);
	while(1)
	{
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		MakeArray_Trans(currentPk);
	}
	
}

// Current Calculation Function Definition
float Current_calc(float ADC_reading, uint8_t pin)
{
	// Initialisation of variable
	float Load_crnt;
	
	// Selecting the gain, based on the pin we are reading from
	// to get proper gain for that channel
 	if (pin == PC3_IsenLG)
 	{
		// Calculate Ipeak for primary gain
		Load_crnt = (ADC_reading - VREF)/G1;
	
 	} 
 	else
 	{
		// Calculate Ipeak for secondary gain 
 		Load_crnt = (ADC_reading - VREF)/G2;
 	}
	
	
	
	return Load_crnt;
}

float CurrentMax(uint8_t pin){
	uint8_t cycles = 9;
	uint16_t adcVal = 0;
	float adcMax = 0;
	float currentPk = 0;
	float adcVin = 0;
	bool wait = true;
			
	setCycleTimer(cycles);
	while(wait)
	{
		// If the overflow is raised, then the 9 cycles
		// must be finished
		if(TIFR0 & (1<<TOV0))
		{
			TIFR0 |= (1<<TOV0);
			wait = false;
		}
				
		// Reading from ADC is assigned to a variable
		adcVal = get_ADCVal(pin);
				
		// ADC reading is then given to the calculate
		// function which will calculate the original
		// Vin that the ADC received.
		adcVin = Calc_ADCVin(adcVal);
		
		
				
		if (adcVin > adcMax)
		{
			//Check for max current value to determine peak
			 adcMax = adcVin;
		} 
	}
	
	//Calculate peak current
	currentPk = Current_calc(adcMax, pin);
	
	// ADC reading is then given to the calculate
	// function which will calculate the original
	// Vin that the ADC received.
	return currentPk;
	
}


// ****************************** VOLTAGE **********************************
// *************************************************************************

// Voltage Calculation Function Definition
float Voltage_calc(float ADC_reading)
{
	// Calculating Vload using the formula based on
	// our PCB circuit.
	float R3 = 100.0;
	float R5 = 39.0;
	float R7 = 10.0;
	float calibration_factor = 0.9898673;
	
	float Load_vltg = (ADC_reading - VREF)*((R3 + R5 + R7)/ R7) * calibration_factor;
	return Load_vltg;
}

// Overall Voltage Function Definition
void Voltagefunct(void)
{
	// The binary representation of the frame for
	// displaying a "V" on the CPLD.
	uint8_t VoltageFrame = 0b11001010;
	
	// Send the "V" frame for 1 second.
	setTimerDelay(1);
	PORTB |= (1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 1 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		UART_Transmit(VoltageFrame);
	}
	
	// Telling the function to calculate the Vload
	// RMS value using PIN C1
	float VLoad = RMSval(PC1_Vsen, 1);
	
	// This RMS value is then given to the MakeArray_Trans
	// function to be transmitted for 2 seconds.
	setTimerDelay(2);
	PORTB &= ~(1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 2 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		MakeArray_Trans(VLoad);
	}
}


// ********************************* POWER *********************************
// *************************************************************************

// Overall Power Function Definition
void Powerfunct(void)
{
	// The binary representation of the frame for
	// displaying a "P" on the CPLD.
	uint8_t PowerFrame = 0b11101000;
	
	// Send the "P" frame for 1 second.
	setTimerDelay(1);
	PORTB |= (1<<5);
	while(1)
	{
		// If the overflow is raised, then the 1 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		UART_Transmit(PowerFrame);
	}
	
	// Checking if the signal on pin 3 is above a certain
	// value. If it is below that value, then we will change
	// to reading from the higher gain circuit.
	uint8_t currentPin = CurrentPinSelect(PC3_IsenLG);
	
	// Calculate current and voltage RMS
	float currentRMS = RMSval(currentPin,2);
	float voltageRMS = RMSval(PC1_Vsen,1);
	
	//
	float powerFactor = cos(CalculatePhaseShift(freq));
	
	// Calculating the power value using all the above values
	// and then converting it into W.
	float power = (currentRMS * voltageRMS * powerFactor);
	
	// Take this power value, and then give it to the energy
	// calculate function to then set the value globally,
	// where then the value is written into eeprom.
	energy = EnergyCalc(power);
	
	// Send the calculated power for 2 seconds.
	setTimerDelay(2);
	PORTB &= ~(1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 2 seconds
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// Keep transmitting if 2 seconds aren't over yet
		MakeArray_Trans(power);
	}
}


// ********************************* OTHER *********************************
// *************************************************************************

// RMS Value calculation Function Definition
float RMSval(uint8_t pin, uint8_t identify)
{
	// Initialising variables
	uint8_t samples = 0;
	uint8_t cycles = 9;
	float RMS_val = 0;
	float sum = 0;
	float calcd_Val = 0;
	
	// Getting a range of values for 5 cycles, then getting
	// the sum of these values' squares and then calculating
	// their average from that
	setCycleTimer(cycles);
	while(1)
	{
		// If the overflow is raised, then the 5 cycles
		//must be finished
		if(TIFR0 & (1<<TOV0))
		{
			TIFR0 |= (1<<TOV0);
			break;
		}
	
		// Reading from ADC, with a given pin
		// is assigned to a variable
		uint16_t adcVal = get_ADCVal(pin);
		
		// ADC reading is then given to the calculate
		// function which will calculate the original
		// Vin that the ADC received.
		float adcVin = Calc_ADCVin(adcVal);
		
		// This switch case will then accordingly use
		// the Vin value to calculate the voltage or
		// current.
		switch (identify)
		{
			case 2:
			// Current
			calcd_Val = Current_calc(adcVin, pin);
			break;
			
			case 1:
			// Calculating the load voltage from the ADC reading
			calcd_Val = Voltage_calc(adcVin);
			break;
		}
		
		// Square the calculated value
		float squaredVal = pow(calcd_Val, 2);
		
		// Get a sum of these squares
		sum = sum + squaredVal;
		samples++;
	}
	
	// Root this sum, and hence get the RMS value,
	// then return the RMS value
	RMS_val = sqrt(sum/samples);
	return RMS_val;
}

// Energy Calculation, Transmission and Storage function
void Energyfunct(void)
{
	// The binary representation of the frame for
	// displaying a "E" on the CPLD.
	uint8_t EnergyFrame = 0b11101010;
	
	// Send the "E" frame for 1 second.
	setTimerDelay(1);
	PORTB |= (1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 1 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		UART_Transmit(EnergyFrame);
	}
	
	// Storing the energy value into eeprom.
	eeprom_add(energy);
	
	// EnergyVal is new energy value
	uint16_t EngyVal = energy;
	
	// This energy value is then given to the MakeArray_Trans
	// function to be transmitted for 2 seconds.
	setTimerDelay(2);
	PORTB &= ~(1<<5);
	
	while(1)
	{
		// If the overflow is raised, then the 2 second
		// count must be over, so break out of the loop.
		if(TIFR1 & (1<<TOV1))
		{
			TIFR1 |= (1<<TOV1);
			break;
		}
		// If the overflow isn't raised then keep transmitting
		MakeArray_Trans(EngyVal);
	}
}


