-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 7 Comparator
-- Decription: Comparator that outputs 1 if the input is "0111" (7)
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_7Comparator IS
	PORT(
		data_in: IN std_logic_vector(3 DOWNTO 0);
		cmp7: OUT std_logic
	);
END Group29_7Comparator;

ARCHITECTURE behaviour of Group29_7Comparator IS
	BEGIN
	compare: process(data_in)
		BEGIN
			if data_in = "0111" then
				cmp7 <= '1';
			else 
				cmp7 <= '0';
			END if;
		END process;
	END behaviour;

