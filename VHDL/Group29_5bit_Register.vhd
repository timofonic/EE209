-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 5-bit Register
-- Decription: 5-bit Register to store BCD number to be displayed and whether decimal point is ON/OFF
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_5bit_Register IS
	PORT (
		load, clk: IN std_logic;
		data_in: IN std_logic_vector(4 DOWNTO 0);
		data_out: OUT std_logic_vector(4 DOWNTO 0)
	);
END Group29_5bit_Register;

ARCHITECTURE behaviour of Group29_5bit_Register IS
-- To synthesise memory, default value
SIGNAL Q_tmp: std_logic_vector (4 DOWNTO 0) := "01101";
BEGIN    
	process (load, clk)
	BEGIN
		if(clk'event AND clk = '1') then
			if(load = '1') then
				Q_tmp <= data_in;
			END if;
		END if;
	END process;
	data_out <= Q_tmp;
END behaviour;
