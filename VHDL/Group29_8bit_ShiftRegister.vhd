----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- Shift Register (8-bit) 
-- Decription: Shift Register that shifts in Rx into the MSB
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
----------------------------------------------------------------------------
----------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_8bit_ShiftRegister IS
	PORT (
		clk, shift_enable, Rx, reset: IN std_logic;
		Q: OUT std_logic_vector(7 DOWNTO 0)
	);
END Group29_8bit_ShiftRegister;

ARCHITECTURE behaviour of Group29_8bit_ShiftRegister IS

--To synthesise memory --
SIGNAL Q_tmp: std_logic_vector(7 DOWNTO 0) := "00000000";

BEGIN
	shift:process(clk, reset, shift_enable)
	BEGIN
		-- If not enabled, display a dash --
		if reset = '1' then
			Q_tmp <= "00000000";
		elsif (clk='1' and clk'event and shift_enable = '1') then 
		-- Shift bits to the right and load Rx input into MSB --
			Q_tmp(7 DOWNTO  0) <= Rx & Q_tmp(7 DOWNTO 1);
		END if;
	END process;	
	-- Assign Q to output signal
	Q <= Q_tmp;
END behaviour;