--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- Moore FSM VHDL Code 
-- Decription: Describes the functionality of the controller to decrypt data and display on a Seven Segment Display
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_FSM IS
	PORT (
		clk: IN std_logic;
		Rx, s_cmp7, s_cmp15, n_cmp7, reset, key_en, key_cmp: IN std_logic;
		b_shift, s_en, n_en, s_reset, n_reset, b_reset, decoder_enable, key_check: OUT std_logic
		);
END Group29_FSM;
		
--Behaviour of the Moore Finite State Machine--
ARCHITECTURE behaviour OF Group29_FSM IS	

--Define all states for the Moore FSM
type state_type IS (idle, start, mid, data, count, stop, passkey,display);
SIGNAL next_state, current_state: state_type := idle;

BEGIN
		
	-- Synchronous state transistion --
	state_transitions: process(clk, reset)
	BEGIN
		if(reset = '1') then
				current_state <= idle;
		elsif clk'event and clk = '1' then
			current_state <= next_state;
		END if;
	END process;
		
	-- Next State Logic for State Transitions --
	next_state_logic: process(current_state, Rx, s_cmp7, s_cmp15, n_cmp7,key_en, key_cmp)
	BEGIN
	case current_state IS
			----------------------------------------------
			-- Idle State --
			----------------------------------------------			
		when idle =>

				--Transistion to next state once input goes low, otherwise stay in idle--
				if Rx = '1' then
					next_state <= idle;
				else
					next_state <= start;
				END if;
				
			----------------------------------------------
			-- Start State --
			----------------------------------------------
		when start =>

				--Transistion to data state once s counter reaches 7 (midpoint of incoming bit), otherwise continue counting--
				if Rx = '0' then
					if s_cmp7 = '1' then
						next_state <= mid;
					else
						next_state <= start;
					END if;
				--Checking that the start bit is still low
				else
					next_state <= idle;
				END if;
			
			
				----------------------------------------------
				-- Mid State --
				----------------------------------------------
			when mid =>						

				-- Transistion to count state if Rx is still low otherwise go back to idle --
					if Rx = '0' then
						next_state <= count;
					else
						next_state <= idle;
					END if;
					
					
			----------------------------------------------
			-- Count State --
			----------------------------------------------
			when count =>						

				--Transistion to data state once count reaches 15 (midpoint of next bit), otherwise continue counting --
				if s_cmp15 = '1' then
					next_state <= data;
				else 
					next_state <= count;
				END if;

			 
			----------------------------------------------
			-- Data State --
			----------------------------------------------
			when data =>						

				--Transistion to stop state once n counter reaches 7 (all 8 bits sampled), otherwise go to count state --
				if n_cmp7 = '1'  then
					next_state <= stop;
				else 
					next_state <= count;
				END if;

				
			----------------------------------------------
			-- Stop State --
			----------------------------------------------
			when stop =>						

				-- Check for passkey once finished sampling --
				if key_en = '1' then
					next_state <= passkey;
				elsif s_cmp15 = '1' then
					next_state <= idle;
				elsif key_cmp = '1' then
					next_state <= display;
				else 
					next_state <= stop;
				END if;	
				
				
			----------------------------------------------
			-- Passkey State --
			----------------------------------------------
			
			-- Check for passkey and transistion back to idle once s counter reaches 15 --
			when passkey =>
				if s_cmp15 = '1' then
					next_state <= idle;
				else	
					next_state <= passkey;
				END if;
			
			
			----------------------------------------------
			-- Display State --
			----------------------------------------------
			
			-- Enables the display and waits for s counter to return to 15 --
			when display =>
				if s_cmp15 = '1' then
					next_state <= idle;
				else 
					next_state <= display;
				END if;
		END case;
	END process;
	
	-- Output Logic for Control Signals --
	output_logic: process(current_state)
		BEGIN
			
			--Default states--
			s_reset <= '0';
			n_reset <= '0';
			b_reset <= '0';
			s_en <= '0';
			n_en <= '0';
			b_shift <= '0';
			decoder_enable <= '0';
			key_check <= '0';
			
			case current_state IS
				
				----------------------------------------------
				-- Idle State --
				----------------------------------------------			
				when idle =>
			
					--Reset and disable both counters and the shift register--
					s_reset <= '1';
					n_reset <= '1';
					b_reset <= '1';
					
					
				----------------------------------------------
				-- Start State --
				----------------------------------------------
				when start =>
			
					--Enable s counter to begin counting--
					s_en <= '1';
		
		
				----------------------------------------------
				-- Mid State --
				----------------------------------------------
				when mid =>						
					
					--Reset s counter--
					s_reset <= '1';
					
				
				----------------------------------------------
				-- Count State --
				----------------------------------------------
				when count =>						
					
					--Enable s counter to begin counting--
					s_en <= '1';

					
				----------------------------------------------
				-- Data State --
				----------------------------------------------
				when data =>						
					
					--Enable n counter to begin counting and shift input Rx into b --
					n_en <= '1';
					b_shift <= '1';
					s_reset <= '1';


				----------------------------------------------
				-- Stop State --
				----------------------------------------------
				when stop =>						
					
					--Allow s to continue counting--
					s_en <= '1';

				----------------------------------------------
				-- Passkey State --
				----------------------------------------------
				when passkey =>
					-- Enable comparator to compare passkey --
					key_check <= '1';
					
					-- Allow s to continue counting--
					s_en <= '1';
				
				----------------------------------------------
				-- Display State --
				----------------------------------------------
				when display =>
					-- Enable display --
					decoder_enable <= '1';
				
					-- Allow s to continue counting--
					s_en <= '1';
					
				END case;
		END process;
END behaviour;
	
