-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 2x4 Display Decoder
-- Decription: Decodes a 2-bit  control input into a 4-bit output to select display on seven segment
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_2x4_Decoder IS
	PORT (
		data_in: IN std_logic_vector (1 DOWNTO 0);
		clk, enable: IN std_logic;
		disp_select: OUT std_logic_vector(3 DOWNTO 0)
	);
END Group29_2x4_Decoder;

ARCHITECTURE behaviour of Group29_2x4_Decoder IS
BEGIN
	process(enable, clk)
	BEGIN 
		if(clk'event AND clk = '1') then
			if(enable = '1')then
				case data_in IS
					-- Select display 0 --
					when "00" =>
						disp_select <= "0001";
						
					-- Select display 1 --
					when "01" =>
						disp_select <= "0010";
						
					-- Select display 2 --	
					when "10" => 
						disp_select <= "0100";
						
					-- Select display 3 --		
					when "11" =>
						disp_select <= "1000";
				END case;
			END if;
		END if;
	END process;
END behaviour;  

