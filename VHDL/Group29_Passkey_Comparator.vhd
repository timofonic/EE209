-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- Passkey Comparator
-- Decription: Checks passkey against a set key
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_Passkey_Comparator IS
	PORT (
		enable: IN std_logic;
		data_in: IN std_logic_vector(7 DOWNTO 0);
		key_cmp: OUT std_logic
	);
END Group29_Passkey_Comparator;

ARCHITECTURE behaviour of Group29_Passkey_Comparator IS
-- To synthesise memory, default value
CONSTANT passkey: std_logic_vector(7 DOWNTO 0) := "00011101";
BEGIN    
	process (enable, data_in)
	BEGIN
		if(enable = '1') then
			if(passkey = data_in) then
				key_cmp <= '1';
			else
				key_cmp <= '0';
			END if;
		END if;
	END process;
END behaviour;
