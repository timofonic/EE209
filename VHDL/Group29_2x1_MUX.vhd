-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 2x1 MUX
-- Decription: Selects either the data stored or a default value depednign on whether deocder is enabled
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_2x1_MUX IS
	PORT (
		s0: IN std_logic;
		data_in: IN std_logic_vector(8 DOWNTO 0);
		data_out: OUT std_logic_vector(8 DOWNTO 0)
	);
END Group29_2x1_MUX;

ARCHITECTURE behaviour of Group29_2x1_MUX IS
-- Default value is 13 (display dash) on display 1
SIGNAL data_tmp: std_logic_vector(8 DOWNTO 0) := "000101101"; 
BEGIN
	process(s0)
	BEGIN 
		if(s0 = '1') then
			data_tmp <= data_in;
		END if;
	END process;
data_out <= data_tmp;
END behaviour;  

