-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- Seven Segment Decoder
-- Decription: 7-segment decoder to select the segments to represent a 4-bit binary number or status
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_SevenSegment_Decoder IS
	PORT (
		disp_select_in: IN std_logic_vector(3 DOWNTO 0);
		BCD_value: IN std_logic_vector(3 DOWNTO 0);
		dp_in: IN std_logic;
		seg7_out: OUT std_logic_vector(6 DOWNTO 0);
		decimal_point: OUT std_logic;
		disp_select_out: OUT std_logic_vector(3 DOWNTO 0)
		
	);
END Group29_SevenSegment_Decoder;

-- 7-segment decoder to select the segments to represent a 4-bit binary number
ARCHITECTURE behaviour of Group29_SevenSegment_Decoder IS
 BEGIN 
	 segment_logic: process(BCD_value, dp_in, disp_select_in)
		 VARIABLE index: natural range 0 TO 15 := 13;
		 TYPE LUT IS ARRAY(0 TO 15) OF STD_LOGIC_VECTOR(6 DOWNTO 0);
		 CONSTANT SEGMENT7_LOOKUP_TABLE: LUT :=
			("1111110", -- Display 0
			 "0110000", -- Display 1
			 "1101101", -- Display 2
			 "1111001", -- Display 3	
			 "0110011", -- Display 4
			 "1011011", -- Display 5 
			 "1011111", -- Display 6
			 "1110000", -- Display 7
			 "1111111", -- Display 8
			 "1110011", -- Display 9
			 "0111110", -- Display character V for voltage
			 "0000110", -- Display character I for current
			 "1100111", -- Display character P for power
			 "0000001", -- Display dash (for null - indicates password not recieved)
			 "1001111", -- Display character E for energy
			 "0000000");-- Display nothing (15 is invalid)
		BEGIN 
			-- Select the correct seven-segment representation of a BCD --
				index := to_integer(unsigned(BCD_value));
				seg7_out <= SEGMENT7_LOOKUP_TABLE(index);	
				
			-- Turn deciaml point on/off --
				decimal_point <= dp_in;
			
			-- Select the display --
				disp_select_out <= disp_select_in;							
	END process;					
END behaviour;

