-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 4-bit Binary Counter
-- Decription: Count from "0000" to "1111" (0-15) with each rising clock edge
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_4bit_Counter IS
	PORT (
		clk, reset, enable: IN std_logic;
		Q: OUT std_logic_vector(3 DOWNTO 0)
	);
END Group29_4bit_Counter;

ARCHITECTURE behaviour of Group29_4bit_Counter IS
	BEGIN
	counter: process(clk,reset)
		VARIABLE count: std_logic_vector(3 DOWNTO 0) := "0000";
		BEGIN
			if (reset = '1') then 
					count := "0000";
			elsif(clk'event AND clk = '1') then 	
			-- Synchronous enable
				if(enable = '1') then
				-- Increment count
					count := count + 1;
				END if;
			END if;
		Q <= count;
	END process;
END behaviour;