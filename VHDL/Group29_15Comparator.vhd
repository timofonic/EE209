-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
-- GROUP 29 WIRELESS ENERGY METER
-- Device: MAX7000AE Family - EPM7064AEL44
-- 15 Comparator
-- Decription: Comparator that outputs 1 if the input is "1111" (15)
-- Authors: Emily Melhuish, Kevin Fernandes, Tyler Harrison, Murali Magesan
-- Date: 15/10/2015
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY Group29_15Comparator IS
	PORT (
		data_in: IN std_logic_vector(3 DOWNTO 0);
		cmp15: OUT std_logic
	);
END Group29_15Comparator;

-- Comparator that outputs 1 if the input is "1110" (14)
ARCHITECTURE behaviour of Group29_15Comparator IS
BEGIN
	compare: process(data_in)
		BEGIN
			if data_in = "1110" then
				cmp15 <= '1';
			else 
				cmp15 <= '0';
			END if;
	END process;
END behaviour;

